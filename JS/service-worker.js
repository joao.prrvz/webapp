/*
	Nom : Pereira Vaz
	Prenom : Joao Victor
	Classe : I.DA-P1A
	Date : 27/05/2024
*/

importScripts('https://storage.googleapis.com/workbox-cdn/releases/6.4.1/workbox-sw.js');

workbox.routing.registerRoute(
	({request}) => request.destination == 'image',
	new workbox.strategies.CacheFirst()
)


